from Tkinter import *

import PIL
from PIL import ImageTk, Image

import STATES

"""
    EMPTY = 1
    BLACK = 2
    WHITE = 3
    BLACK_KING = 4
    WHITE_KING = 5
"""


class CheckerSquare(object, Frame):
    def __init__(self, parent,main, id, id_x, id_y, color, **kw):
        Frame.__init__(self, parent,height=65, width=65,bg=color)
        self.id = id
        self.main_inst=main
        self.squares=self.main_inst.squares
        self.id_x = id_x
        self.id_y = id_y
        self.color=None
        self.coin_type=None
        self.state = STATES.coins.EMPTY
        self.coin_frame= Frame(self)
        self.coin_label = Label(self.coin_frame)
        self.coin_frame.pack()



    def callback(self, event):
        if self.main_inst.continue_capture:
            if self in self.main_inst.highlighted_squares:
                self.main_inst.clearAllHighlights()
                self.main_inst.moveChecker(self.main_inst.selected_checker, self,True)
        else:
            if self.main_inst.forced_to_capture:
                if self in self.main_inst.highlighted_squares:
                    self.main_inst.clearAllHighlights()
                    self.main_inst.moveChecker(self.main_inst.selected_checker, self, True)
                    if self.isCapturePossible():
                        self.highlightCaptureMoves()
                else:
                    self.main_inst.clearAllHighlights()
                    if self.isCapturePossible():
                        self.highlightCaptureMoves()
            else:
                if self in self.main_inst.highlighted_squares:
                    self.main_inst.clearAllHighlights()
                    self.main_inst.moveChecker(self.main_inst.selected_checker, self)
                elif self.main_inst.players[self.main_inst.turn].getColor() == self.color:
                    self.main_inst.clearAllHighlights()
                    self.highlightPossibleMoves(self.main_inst.players[self.main_inst.turn].getSide())

    def findSquare(self, id_x, id_y):
        for i in range(0, 32):
            state = self.squares[i]
            if (state.id_x == id_x) & (state.id_y == id_y):
                return state
        return None

    def highlight(self,value):
        if value:
            image = PIL.Image.open("icons/highlight.png").convert("RGBA")
            photo = PIL.ImageTk.PhotoImage(image)
            self.make_label(photo)
        else:
            self.make_label(None)

    def make_label(self, img):
        label = self.coin_label
        label.configure(image=img)
        label.image=img
        label.bind("<Button-1>", self.callback)
        label.pack(expand=False, side=BOTTOM)

    def setState(self, state):
        self.state = state
        photo=None
        if state != STATES.coins.EMPTY:
            if state == STATES.coins.BLACK:
                image=PIL.Image.open("icons/coin_black.png").convert("RGBA")
                photo = PIL.ImageTk.PhotoImage(image)
                self.color=STATES.color.BLACK
                self.coin_type = STATES.cointype.NORMAL
            elif state == STATES.coins.WHITE:
                image = PIL.Image.open("icons/coin_red.png").convert("RGBA")
                photo = PIL.ImageTk.PhotoImage(image)
                self.color = STATES.color.WHITE
                self.coin_type = STATES.cointype.NORMAL
            elif state == STATES.coins.BLACK_KING:
                image = PIL.Image.open("icons/coin_black_king.png").convert("RGBA")
                photo = PIL.ImageTk.PhotoImage(image)
                self.color = STATES.color.BLACK
                self.coin_type = STATES.cointype.KING
            elif state == STATES.coins.WHITE_KING:
                image = PIL.Image.open("icons/coin_red_king.png").convert("RGBA")
                photo = PIL.ImageTk.PhotoImage(image)
                self.color = STATES.color.WHITE
                self.coin_type = STATES.cointype.KING

            self.make_label(photo)
            if self.main_inst.players[self.main_inst.turn].getSide() == STATES.side.TOP:
                if self.id_x==7:
                    if self.getState()==STATES.coins.BLACK:
                        self.setState(STATES.coins.BLACK_KING)
                    elif self.getState()==STATES.coins.WHITE:
                        self.setState(STATES.coins.WHITE_KING)
                    self.coin_type = STATES.cointype.KING
            elif self.main_inst.players[self.main_inst.turn].getSide() == STATES.side.BOTTOM:
                if self.id_x==0:
                    if self.getState()==STATES.coins.BLACK:
                        self.setState(STATES.coins.BLACK_KING)
                    elif self.getState()==STATES.coins.WHITE:
                        self.setState(STATES.coins.WHITE_KING)
                    self.coin_type = STATES.cointype.KING
        else:
            self.color=STATES.color.EMPTY
            self.make_label(photo)

    def getState(self):
        return self.state

    def highlightPossibleMoves(self,side):
        self.main_inst.highlighted_squares = []
        if self.coin_type == STATES.cointype.KING:
            sq = self.findSquare(self.id_x + 1, self.id_y + 1)
            if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                self.main_inst.highlighted_squares.append(sq)
                sq.highlight(True)
                self.main_inst.selected_a_checker = True
                self.main_inst.selected_checker=self
            sq = self.findSquare(self.id_x + 1, self.id_y - 1)
            if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                self.main_inst.highlighted_squares.append(sq)
                sq.highlight(True)
                self.main_inst.selected_a_checker = True
                self.main_inst.selected_checker = self
            sq = self.findSquare(self.id_x - 1, self.id_y + 1)
            if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                self.main_inst.highlighted_squares.append(sq)
                sq.highlight(True)
                self.main_inst.selected_a_checker = True
                self.main_inst.selected_checker = self
            sq = self.findSquare(self.id_x - 1, self.id_y - 1)
            if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                self.main_inst.highlighted_squares.append(sq)
                sq.highlight(True)
                self.main_inst.selected_a_checker = True
                self.main_inst.selected_checker = self
        else:
            if side == STATES.side.TOP:
                sq = self.findSquare(self.id_x + 1, self.id_y + 1)
                if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                    self.main_inst.highlighted_squares.append(sq)
                    sq.highlight(True)
                    self.main_inst.selected_a_checker = True
                    self.main_inst.selected_checker = self
                sq = self.findSquare(self.id_x + 1, self.id_y - 1)
                if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                    self.main_inst.highlighted_squares.append(sq)
                    sq.highlight(True)
                    self.main_inst.selected_a_checker = True
                    self.main_inst.selected_checker = self
            elif side == STATES.side.BOTTOM:
                sq = self.findSquare(self.id_x - 1, self.id_y + 1)
                if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                    self.main_inst.highlighted_squares.append(sq)
                    sq.highlight(True)
                    self.main_inst.selected_a_checker = True
                    self.main_inst.selected_checker = self
                sq = self.findSquare(self.id_x - 1, self.id_y - 1)
                if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                    self.main_inst.highlighted_squares.append(sq)
                    sq.highlight(True)
                    self.main_inst.selected_a_checker = True
                    self.main_inst.selected_checker = self

    def highlightCaptureMoves(self):
        opColor = None
        self.main_inst.highlighted_squares=[]
        side = self.main_inst.players[self.main_inst.turn].getSide()
        if self.color == STATES.color.BLACK:
            opColor = STATES.color.WHITE
        elif self.color == STATES.color.WHITE:
            opColor = STATES.color.BLACK

        if self.coin_type == STATES.cointype.KING:
            sq = self.findSquare(self.id_x + 1, self.id_y + 1)
            if (sq is not None) and (sq.color == opColor):
                sq2 = self.findSquare(self.id_x + 2, self.id_y + 2)
                if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                    self.main_inst.highlighted_squares.append(sq2)
                    sq2.highlight(True)
                    self.main_inst.selected_a_checker = True
                    self.main_inst.selected_checker = self
            sq = self.findSquare(self.id_x + 1, self.id_y - 1)
            if (sq is not None) and (sq.color == opColor):
                sq2 = self.findSquare(self.id_x + 2, self.id_y - 2)
                if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                    self.main_inst.highlighted_squares.append(sq2)
                    sq2.highlight(True)
                    self.main_inst.selected_a_checker = True
                    self.main_inst.selected_checker = self
            sq = self.findSquare(self.id_x - 1, self.id_y + 1)
            if (sq is not None) and (sq.color == opColor):
                sq2 = self.findSquare(self.id_x - 2, self.id_y + 2)
                if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                    self.main_inst.highlighted_squares.append(sq2)
                    sq2.highlight(True)
                    self.main_inst.selected_a_checker = True
                    self.main_inst.selected_checker = self
            sq = self.findSquare(self.id_x - 1, self.id_y - 1)
            if (sq is not None) and (sq.color == opColor):
                sq2 = self.findSquare(self.id_x - 2, self.id_y - 2)
                if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                    self.main_inst.highlighted_squares.append(sq2)
                    sq2.highlight(True)
                    self.main_inst.selected_a_checker = True
                    self.main_inst.selected_checker = self
        else:
            if side == STATES.side.TOP:
                sq = self.findSquare(self.id_x + 1, self.id_y + 1)
                if (sq is not None) and (sq.color == opColor):
                    sq2 = self.findSquare(self.id_x + 2, self.id_y + 2)
                    if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                        self.main_inst.highlighted_squares.append(sq2)
                        sq2.highlight(True)
                        self.main_inst.selected_a_checker = True
                        self.main_inst.selected_checker = self
                sq = self.findSquare(self.id_x + 1, self.id_y - 1)
                if (sq is not None) and (sq.color == opColor):
                    sq2 = self.findSquare(self.id_x + 2, self.id_y - 2)
                    if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                        self.main_inst.highlighted_squares.append(sq2)
                        sq2.highlight(True)
                        self.main_inst.selected_a_checker = True
                        self.main_inst.selected_checker = self
            elif side == STATES.side.BOTTOM:
                sq = self.findSquare(self.id_x - 1, self.id_y + 1)
                if (sq is not None) and (sq.color == opColor):
                    sq2 = self.findSquare(self.id_x - 2, self.id_y + 2)
                    if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                        self.main_inst.highlighted_squares.append(sq2)
                        sq2.highlight(True)
                        self.main_inst.selected_a_checker = True
                        self.main_inst.selected_checker = self
                sq = self.findSquare(self.id_x - 1, self.id_y - 1)
                if (sq is not None) and (sq.color == opColor):
                    sq2 = self.findSquare(self.id_x - 2, self.id_y - 2)
                    if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                        self.main_inst.highlighted_squares.append(sq2)
                        sq2.highlight(True)
                        self.main_inst.selected_a_checker = True
                        self.main_inst.selected_checker = self

    def isCapturePossible(self):
        opColor=None
        flag= False
        side=None
        if self.color==STATES.color.BLACK:
            if self.main_inst.white_on_top:
                side=STATES.side.BOTTOM
            else:
                side=STATES.side.TOP
            opColor=STATES.color.WHITE
        elif self.color==STATES.color.WHITE:
            if self.main_inst.white_on_top:
                side=STATES.side.TOP
            else:
                side=STATES.side.BOTTOM
            opColor=STATES.color.BLACK

        if self.coin_type == STATES.cointype.KING:
            sq = self.findSquare(self.id_x + 1, self.id_y + 1)
            if (sq is not None) and (sq.color == opColor):
                sq2=self.findSquare(self.id_x + 2, self.id_y + 2)
                if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                    flag = True
            sq = self.findSquare(self.id_x + 1, self.id_y - 1)
            if (sq is not None) and (sq.color == opColor):
                sq2=self.findSquare(self.id_x + 2, self.id_y - 2)
                if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                    flag = True
            sq = self.findSquare(self.id_x - 1, self.id_y + 1)
            if (sq is not None) and (sq.color == opColor):
                sq2=self.findSquare(self.id_x - 2, self.id_y + 2)
                if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                    flag = True
            sq = self.findSquare(self.id_x - 1, self.id_y - 1)
            if (sq is not None) and (sq.color == opColor):
                sq2=self.findSquare(self.id_x - 2, self.id_y - 2)
                if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                    flag = True
        else:
            if side == STATES.side.TOP:
                sq = self.findSquare(self.id_x + 1, self.id_y + 1)
                if (sq is not None) and (sq.color == opColor):
                    sq2= self.findSquare(self.id_x + 2,self.id_y + 2)
                    if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                        flag = True
                sq = self.findSquare(self.id_x + 1, self.id_y - 1)
                if (sq is not None) and (sq.color == opColor):
                    sq2= self.findSquare(self.id_x + 2,self.id_y - 2)
                    if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                        flag = True
            elif side == STATES.side.BOTTOM:
                sq = self.findSquare(self.id_x - 1, self.id_y + 1)
                if (sq is not None) and (sq.color == opColor):
                    sq2= self.findSquare(self.id_x - 2,self.id_y + 2)
                    if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                        flag = True
                sq = self.findSquare(self.id_x - 1, self.id_y - 1)
                if (sq is not None) and (sq.color == opColor):
                    sq2= self.findSquare(self.id_x - 2,self.id_y - 2)
                    if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                        flag = True
        return flag

    def isUnderThreat(self):
        opColor=None
        opSide=None
        flag = False

        if self.color==STATES.color.BLACK:
            if self.main_inst.white_on_top:
                opSide=STATES.side.TOP
            else:
                opSide=STATES.side.BOTTOM
            opColor=STATES.color.WHITE
        elif self.color==STATES.color.WHITE:
            if self.main_inst.white_on_top:
                opSide=STATES.side.BOTTOM
            else:
                opSide=STATES.side.TOP
            opColor=STATES.color.BLACK

        if opSide==STATES.side.TOP:
            sq = self.findSquare(self.id_x - 1,self.id_y - 1)
            if (sq is not None) and (sq.color == opColor):
                sq2=self.findSquare(self.id_x + 1,self.id_y + 1)
                if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                    return True

            sq = self.findSquare(self.id_x - 1, self.id_y + 1)
            if (sq is not None) and (sq.color == opColor):
                sq2 = self.findSquare(self.id_x + 1, self.id_y - 1)
                if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                    return True

            sq = self.findSquare(self.id_x + 1, self.id_y + 1)
            if (sq is not None) and (sq.color == opColor):
                    if sq.coin_type==STATES.cointype.KING:
                        sq2 = self.findSquare(self.id_x - 1, self.id_y - 1)
                        if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                            return True

            sq = self.findSquare(self.id_x + 1, self.id_y + 1)
            if (sq is not None) and (sq.color == opColor):
                    if sq.coin_type==STATES.cointype.KING:
                        sq2 = self.findSquare(self.id_x - 1, self.id_y - 1)
                        if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                            return True
        else:
            sq = self.findSquare(self.id_x - 1, self.id_y + 1)
            if (sq is not None) and (sq.color == opColor):
                if sq.coin_type == STATES.cointype.KING:
                    sq2 = self.findSquare(self.id_x + 1, self.id_y - 1)
                    if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                        return True

            sq = self.findSquare(self.id_x - 1, self.id_y - 1)
            if (sq is not None) and (sq.color == opColor):
                if sq.coin_type == STATES.cointype.KING:
                    sq2 = self.findSquare(self.id_x + 1, self.id_y + 1)
                    if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                        return True
            sq = self.findSquare(self.id_x + 1, self.id_y + 1)
            if (sq is not None) and (sq.color == opColor):
                sq2 = self.findSquare(self.id_x - 1, self.id_y - 1)
                if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                    return True

            sq = self.findSquare(self.id_x + 1, self.id_y - 1)
            if (sq is not None) and (sq.color == opColor):
                sq2 = self.findSquare(self.id_x - 1, self.id_y + 1)
                if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                    return True
        return False

    def isMovable(self,side):
        if self.isCapturePossible():
            return True
        if self.coin_type == STATES.cointype.KING:
            sq = self.findSquare(self.id_x + 1, self.id_y + 1)
            if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                return True
            sq = self.findSquare(self.id_x + 1, self.id_y - 1)
            if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                return True
            sq = self.findSquare(self.id_x - 1, self.id_y + 1)
            if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                return True
            sq = self.findSquare(self.id_x - 1, self.id_y - 1)
            if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                return True
        else:
            if side == STATES.side.TOP:
                sq = self.findSquare(self.id_x + 1, self.id_y + 1)
                if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                    return True
                sq = self.findSquare(self.id_x + 1, self.id_y - 1)
                if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                    return True
            elif side == STATES.side.BOTTOM:
                sq = self.findSquare(self.id_x - 1, self.id_y + 1)
                if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                    return True
                sq = self.findSquare(self.id_x - 1, self.id_y - 1)
                if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                    return True
        return False