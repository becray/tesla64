import random
import tkMessageBox

import STATES


class Player():

    def __init__(self,id,color,type,side,main_inst):
        self.main_inst=main_inst
        self.squares=main_inst.squares
        self.type=type
        self.side=side
        self.id=id
        self.color=color
        self.moves=[]
        if self.color== STATES.color.WHITE:
            if self.side == STATES.side.TOP:
                self.main_inst.white_on_top=True
            else:
                self.main_inst.white_on_top = False

    def getType(self):
        return self.type

    def getSide(self):
        return self.side

    def getId(self):
        return self.id

    def getColor(self):
        return self.color

    def arrangeCoins(self):
        self.main_inst.turn=self.id
        if self.side==STATES.side.TOP:
            if (self.color == STATES.color.WHITE):
                for i in range(0, 12):
                    self.squares[i].setState(STATES.coins.WHITE)
            elif(self.color==STATES.color.BLACK):
                for i in range(0, 12):
                    self.squares[i].setState(STATES.coins.BLACK)

        elif self.side==STATES.side.BOTTOM:
            if (self.color == STATES.color.WHITE):
                for i in range(20, 32):
                    self.squares[i].setState(STATES.coins.WHITE)
            elif(self.color==STATES.color.BLACK):
                for i in range(20, 32):
                    self.squares[i].setState(STATES.coins.BLACK)

    def findSquare(self, id_x, id_y):
        for i in range(0, 32):
            state = self.squares[i]
            if (state.id_x == id_x) & (state.id_y == id_y):
                return state
        return None

    def appendPossibleMovesOfChecker(self, square, movesArray,capture=False):
        if capture:
            opColor = None
            side = self.side
            if self.color == STATES.color.BLACK:
                opColor = STATES.color.WHITE
            elif self.color == STATES.color.WHITE:
                opColor = STATES.color.BLACK

            if square.coin_type == STATES.cointype.KING:
                sq = self.findSquare(square.id_x + 1, square.id_y + 1)
                if (sq is not None) and (sq.color == opColor):
                    sq2 = self.findSquare(square.id_x + 2, square.id_y + 2)
                    if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                        movesArray.append([square, sq2])
                sq = self.findSquare(square.id_x + 1, square.id_y - 1)
                if (sq is not None) and (sq.color == opColor):
                    sq2 = self.findSquare(square.id_x + 2, square.id_y - 2)
                    if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                        movesArray.append([square, sq2])
                sq = self.findSquare(square.id_x - 1, square.id_y + 1)
                if (sq is not None) and (sq.color == opColor):
                    sq2 = self.findSquare(square.id_x - 2, square.id_y + 2)
                    if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                        movesArray.append([square, sq2])
                sq = self.findSquare(square.id_x - 1, square.id_y - 1)
                if (sq is not None) and (sq.color == opColor):
                    sq2 = self.findSquare(square.id_x - 2, square.id_y - 2)
                    if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                        movesArray.append([square, sq2])
            else:
                if side == STATES.side.TOP:
                    sq = self.findSquare(square.id_x + 1, square.id_y + 1)
                    if (sq is not None) and (sq.color == opColor):
                        sq2 = self.findSquare(square.id_x + 2, square.id_y + 2)
                        if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                            movesArray.append([square, sq2])
                    sq = self.findSquare(square.id_x + 1, square.id_y - 1)
                    if (sq is not None) and (sq.color == opColor):
                        sq2 = self.findSquare(square.id_x + 2, square.id_y - 2)
                        if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                            movesArray.append([square, sq2])
                elif side == STATES.side.BOTTOM:
                    sq = self.findSquare(square.id_x - 1, square.id_y + 1)
                    if (sq is not None) and (sq.color == opColor):
                        sq2 = self.findSquare(square.id_x - 2, square.id_y + 2)
                        if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                            movesArray.append([square, sq2])
                    sq = self.findSquare(square.id_x - 1, square.id_y - 1)
                    if (sq is not None) and (sq.color == opColor):
                        sq2 = self.findSquare(square.id_x - 2, square.id_y - 2)
                        if (sq2 is not None) and (sq2.state == STATES.coins.EMPTY):
                            movesArray.append([square, sq2])
        else:
            side = self.side
            if square.coin_type == STATES.cointype.KING:
                sq = self.findSquare(square.id_x + 1, square.id_y + 1)
                if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                    movesArray.append([square, sq])
                sq = self.findSquare(square.id_x + 1, square.id_y - 1)
                if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                    movesArray.append([square, sq])
                sq = self.findSquare(square.id_x - 1, square.id_y + 1)
                if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                    movesArray.append([square, sq])
                sq = self.findSquare(square.id_x - 1, square.id_y - 1)
                if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                    movesArray.append([square, sq])
            else:
                if side == STATES.side.TOP:
                    sq = self.findSquare(square.id_x + 1, square.id_y + 1)
                    if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                        movesArray.append([square, sq])
                    sq = self.findSquare(square.id_x + 1, square.id_y - 1)
                    if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                        movesArray.append([square, sq])
                elif side == STATES.side.BOTTOM:
                    sq = self.findSquare(square.id_x - 1, square.id_y + 1)
                    if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                        movesArray.append([square, sq])
                    sq = self.findSquare(square.id_x - 1, square.id_y - 1)
                    if (sq is not None) and (sq.state == STATES.coins.EMPTY):
                        movesArray.append([square, sq])

    def findPossibleMoves(self):
        self.moves=[]
        for sq in self.squares:
            if sq.color==self.color and sq.isMovable(self.side):
                self.appendPossibleMovesOfChecker(sq,self.moves)

    def findCaptureMoves(self):
        self.moves = []
        for sq in self.squares:
            if sq.color == self.color and sq.isCapturePossible():
                self.appendPossibleMovesOfChecker(sq, self.moves, True)

    def isGameOver(self):
        for sq in self.squares:
            if sq.color==self.color and sq.isMovable(self.side):
                return False
        return True

    def myTurn(self):
        print "Player #%d's Turn (%s)" %(self.id,self.type)
        if not self.isGameOver():
            if not self.main_inst.game_draw:
                if self.type == STATES.player.HUMAN:
                    self.main_inst.force_to_capture = self.main_inst.isCapturePossibleForPlayer(self.id)
                    if self.main_inst.force_to_capture:
                        print "a capture is available and %s is forced to take it" % self.color
                        self.main_inst.forced_to_capture = True
                else:
                    if self.main_inst.isCapturePossibleForPlayer(self.id):
                        if self.main_inst.continue_capture:
                            self.moves = []
                            self.appendPossibleMovesOfChecker(self.main_inst.forced_coin, self.moves, True)
                            # no_of_moves = len(self.moves)
                            # x = self.selectRandomMove(no_of_moves)
                            x = self.selectBestMove(True)
                            self.main_inst.moveChecker(self.moves[x][0], self.moves[x][1], True)
                        else:
                            self.findCaptureMoves()
                            # no_of_moves = len(self.moves)
                            # x = self.selectRandomMove(no_of_moves)
                            x = self.selectBestMove(True)
                            self.main_inst.moveChecker(self.moves[x][0], self.moves[x][1], True)
                    else:
                        self.findPossibleMoves()
                        # no_of_moves = len(self.moves)
                        # x = self.selectRandomMove(no_of_moves)
                        x = self.selectBestMove(False)
                        self.main_inst.moveChecker(self.moves[x][0], self.moves[x][1], False)
            else:
                print "Game ends draw..."
                print "Total no.of moves = ", self.main_inst.total_move_count
                if self.main_inst.train_mode:
                    self.main_inst.train_game_draws = self.main_inst.train_game_draws + 1
                else:
                    tkMessageBox.showinfo("Game Over!!!", "Draw.." )
                self.main_inst.gameFrame.destroy()
                self.main_inst.game_window.withdraw()
        else:
            print "Game Over...Player #%d Lost  (%s)..." %(self.id,self.color)
            print "Total no.of moves = ",self.main_inst.total_move_count
            we_won=False
            if self.id==1:
                we_won=True
            self.addExperienceToStateGraph(self.main_inst.board_pos_array,self.main_inst.factors_array,we_won)
            if self.main_inst.train_mode:
                self.main_inst.total_train_moves=self.main_inst.total_train_moves+self.main_inst.total_move_count
                if we_won:
                    self.main_inst.train_game_wins = self.main_inst.train_game_wins + 1
            else:
                tkMessageBox.showinfo("Game Over!!!", "Player %d Lost  (%s)..." % (self.id, self.color))
            self.main_inst.gameFrame.destroy()
            self.main_inst.game_window.withdraw()

    def selectRandomMove(self,n):
        x = random.randint(0, n - 1)
        return x

    def selectBestMove(self,cap):
        move = self.main_inst.moveCheckerVirtually(self.moves[0][0], self.moves[0][1], cap)
        best_score=self.findScore(move)
        best_move=[0]
        for i in range(1,len(self.moves)):
            move=self.main_inst.moveCheckerVirtually(self.moves[i][0],self.moves[i][1],cap)
            score=self.findScore(move)
            if score>best_score:
                best_score=score
                best_move=[]
                best_move.append(i)
            elif score==best_score:
                best_move.append(i)
        #from best moves select a random best move
        print ("best move clash count = %d ")%(len(best_move))
        if len(best_move)>1:
            self.main_inst.random_train_moves = self.main_inst.random_train_moves + 1
        x = random.randint(0, len(best_move)-1)
        return best_move[x]

    def addExperienceToStateGraph(self,board_pos_array,factors,we_won):
        i=0
        for pos in board_pos_array:
            score = self.calculateScore(i, board_pos_array[i],factors[i], we_won)
            self.main_inst.db.insert_board_pos(score,pos)
            i=i+1
        self.main_inst.db.commit()

    def calculateScore(self, i, pos,factors, we_won):
        default_min_score=0.1

        a=0.13
        b=0.14
        c=-0.11
        d=-0.12
        e=-0.2
        f=0.2
        g=-0.05
        h=0.05
        j=0.5

        my_coins=0
        my_kings=0
        opponent_coins=0
        opponent_kings=0
        my_coins_under_threat=factors[0]
        opponent_coins_under_threat=factors[1]
        my_immovable_coins=factors[2]
        opponent_immovable_coins=factors[3]
        jumps_taken=factors[4]

        we_are_white=False
        T=self.main_inst.total_move_count

        if self.main_inst.players[0].color==STATES.color.WHITE:
            we_are_white=True
        temp=int("".join(pos))
        while(temp>0):
            x=temp%10
            if x==1:
                my_coins=my_coins+1
            elif x==2:
                my_kings=my_kings+1
            elif x==3:
                opponent_coins=opponent_coins+1
            elif x==4:
                opponent_kings=opponent_kings+1
            temp=temp/10

        score=float((i+1)*(a*my_coins + b*my_kings + c*opponent_coins + d*opponent_kings + e*my_coins_under_threat + f*opponent_coins_under_threat + g*my_immovable_coins + h*opponent_immovable_coins + j*jumps_taken))/float(T)
        if score==0:
            score=(i+1)*default_min_score

        #score = score * 0.1

        if we_are_white:
            if i%2==0:
                if we_won:
                    return score
                else:
                    return -score
            else:
                if we_won:
                    return -score
                else:
                    return score
        else:
            if i%2==0:
                if we_won:
                    return -score
                else:
                    return score
            else:
                if we_won:
                    return score
                else:
                    return -score

    def findScore(self,pos):
        return self.main_inst.db.find_score_by_matrix(pos)
