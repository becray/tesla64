import os
import sqlite3

class DB_manager():

    def __init__(self):
        db = "databases/state_graph.db"
        dir = os.path.dirname(db)
        if not os.path.exists(dir):
            os.makedirs(dir)

        self.conn = sqlite3.connect('databases/state_graph.db')
        self.conn.execute('''CREATE TABLE IF NOT EXISTS STATE_GRAPH
                   (ID INTEGER  UNIQUE  NOT NULL,
                   SCORE           FLOAT,
                   MATRIX          TEXT     PRIMARY KEY NOT NULL);''')

    def insert_board_pos(self,score,board_pos):
        pos="".join(board_pos)
        ins_query = "INSERT OR IGNORE INTO STATE_GRAPH (ID, MATRIX) VALUES ((SELECT IFNULL(MAX(ID), 0) + 1 FROM STATE_GRAPH), '%s' );" %(pos)
        up_query = "UPDATE STATE_GRAPH SET SCORE = (SELECT IFNULL ((SELECT SCORE FROM STATE_GRAPH WHERE  MATRIX = '%s'),0)+ %.3f) WHERE MATRIX = '%s'"%(pos,score,pos)
        self.conn.execute(ins_query)
        self.conn.execute(up_query)

    def commit(self):
        self.conn.commit()

    def find_score_by_matrix(self,board_pos):
        pos="".join(board_pos)
        query = "SELECT SCORE FROM STATE_GRAPH WHERE MATRIX = '%s'" % (pos)
        cursor = self.conn.execute(query)
        ans=cursor.fetchone()
        if ans==None:
            return float(0)
        else:
            return ans[0]


    def find_all_board_pos(self):
        query = "SELECT * FROM STATE_GRAPH"
        cursor = self.conn.execute(query)
        for row in cursor:
            print "ID = ", row[0]
            print "SCORE = ", row[1]
            print "MATRIX = ", int(row[2])
            print "\n"
            x = int(row[2])
            while x>0:
                print x%10
                x/=10