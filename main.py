import cProfile
import tkMessageBox
import ttk
from Tkconstants import HORIZONTAL
from Tkinter import Button,Frame,Tk, Toplevel, Entry, Label

import STATES
from CheckerSqaure import CheckerSquare
from DB_manager import DB_manager
from Player import Player


class main:

    def __init__(self):
        self.window = Tk()
        self.homeFrame = Frame(self.window)
        self.gameFrame=None
        self.game_window=None
        self.squares = []
        self.players= []
        self.highlighted_squares=[]
        self.forced_to_capture=False
        self.continue_capture=False
        self.forced_coin=None
        self.turn=0
        self.board_pos_array=[]
        self.factors_array=[]
        self.jumps_taken=0
        self.total_move_count=0
        self.white_on_top=True
        self.selected_a_checker=False
        self.selected_checker=None
        self.createHomeScreen()
        self.db= DB_manager()
        self.train_mode=False
        self.train_game_count=0
        self.train_iterations=5
        self.train_game_wins=0
        self.train_game_draws=0
        self.longest_train_game_length=0
        self.total_train_moves=0
        self.random_train_moves=0
        self.game_draw=False
        self.game_draw_move_count=0  #50 continous stale moves will make game Draw
        self.window.mainloop()

    def prepBoard(self):
        self.squares=[]
        color = "white"
        id = 0
        for i in range(0, 8):
            for j in range(0, 8):
                if (color == "white"):
                    sq = CheckerSquare(self.gameFrame,self, id, i, j, color)
                    id = id + 1
                    sq.grid(row=i, column=j)
                    self.squares.append(sq)
                    color = "black"
                elif (color == "black"):
                    dummy = Frame(self.gameFrame, height=65, width=65, bg=color)
                    dummy.grid(row=i, column=j)
                    color = "white"
            if (color == "black"):
                color = "white"
            elif (color == "white"):
                color = "black"

    def initiatePlayers(self,p1, p2):
        self.players=[]
        self.players.append(Player(0, STATES.color.BLACK, p1, STATES.side.TOP, self))
        self.players.append(Player(1, STATES.color.WHITE, p2, STATES.side.BOTTOM, self))
        self.players[0].arrangeCoins()
        self.players[1].arrangeCoins()
        if(self.players[0].getColor()==STATES.color.BLACK):
            self.turn=0
            self.exchangeTurn()
            self.players[self.turn].myTurn()
        else:
            self.turn=1
            self.exchangeTurn()
            self.players[self.turn].myTurn()

    def createGameScreen(self):
        #self.homeFrame.pack_forget()
        self.game_draw_move_count=0
        self.game_draw=False
        self.total_move_count = 0
        self.board_pos_array=[]
        self.factors_array=[]
        self.jumps_taken=0
        self.game_window= Toplevel()
        self.game_window.title("Tesla64")
        self.gameFrame = Frame(self.game_window)
        self.gameFrame.pack()
        self.prepBoard()
        if self.train_mode:
            self.initiatePlayers(STATES.player.COMPUTER, STATES.player.COMPUTER)
        else:
            self.initiatePlayers(STATES.player.COMPUTER, STATES.player.HUMAN)


    def createHomeScreen(self):
        #self.gameFrame.pack_forget()
        play = Button(self.homeFrame, text="PLAY", command=self.callback)
        play.grid(row=0, column=0, padx=50, pady=50)
        train = Button(self.homeFrame, text="Train",command=self.train_callback)
        train.grid(row=0, column=1, padx=50, pady=50)
        self.homeFrame.pack()

    def callback(self):
        self.createGameScreen()

    def train_start_callback(self):
        pr = cProfile.Profile()
        pr.enable()
        self.train_mode = True
        self.train_game_wins=0
        self.train_game_draws=0
        self.total_train_moves = 0
        self.random_train_moves = 0
        self.train_iterations = int(self.e.get())
        self.progressbar["maximum"] = self.train_iterations
        #self.progressbar.maximum=self.train_iterations
        self.progressbar.start()
        self.b.configure(state='disabled')
        self.e.configure(state='disabled')
        self.train_game_count=self.train_iterations
        for i in range(0,self.train_iterations):
            if self.longest_train_game_length < self.total_move_count:
                self.longest_train_game_length=self.total_move_count
            self.createGameScreen()
            self.progressbar["value"] = i
            self.progressbar.update()
        self.b.configure(state='normal')
        self.e.configure(state='normal')
        self.progressbar.stop()
        self.ipwindow.withdraw()
        print self.total_train_moves
        print self.random_train_moves
        print "Train statistics ... random move percentile = %.3f%%" %(float(self.random_train_moves)*100/float(self.total_train_moves))
        tkMessageBox.showinfo("Training Complete...", "Games Played : %d\nGames Won : %d\nGame Lost : %d\nGames Draw : %d\nLongest Game length : %d moves" % (self.train_game_count, self.train_game_wins,self.train_game_count-self.train_game_wins-self.train_game_draws,self.train_game_draws,self.longest_train_game_length))
        self.train_mode=False
        pr.disable()
        pr.print_stats(sort="tottime")
    def train_callback(self):
        self.ipwindow=Toplevel()
        self.ipwindow.title("Train Tesla64")
        self.w = Label(self.ipwindow, text="Enter number of games to train.")
        self.w.grid(row=0,column=0,padx=30,pady=10)
        self.e = Entry(self.ipwindow)
        self.e.grid(row=1,column=0,padx=30,pady=10)
        self.b = Button(self.ipwindow, text="Train", width=10, command=self.train_start_callback)
        self.b.grid(row=2,column=0,padx=30,pady=10)
        self.progressbar = ttk.Progressbar(self.ipwindow, orient=HORIZONTAL,length=400,mode='determinate')
        self.progressbar.grid(row=4, column=0, padx=30, pady=10)

    def clearAllHighlights(self):
        for state in self.highlighted_squares:
            state.highlight(False)
        self.highlighted_squares = []

    def findSquare(self, id_x, id_y):
        for i in range(0, 32):
            state = self.squares[i]
            if (state.id_x == id_x) & (state.id_y == id_y):
                return state
        return None

    def capture(self,prior,next):
        x=(prior.id_x+next.id_x)/2
        y=(prior.id_y+next.id_y)/2
        sq=self.findSquare(x,y)
        sq.setState(STATES.coins.EMPTY)

    def moveChecker(self,prior,next,isCapture=False):
        print "move #%d   = player %d moved from %d x %d    to   %d x %d " %(self.total_move_count,self.turn,prior.id_x,prior.id_y,next.id_x,next.id_y)
        if isCapture:
            self.game_draw_move_count = 0
        else:
            if prior.state == STATES.coins.BLACK_KING or prior.state == STATES.coins.WHITE_KING:
                # king play
                self.game_draw_move_count = self.game_draw_move_count + 1
                if self.game_draw_move_count >= 100:  # when each player makes continuous 50 stale moves, game is draw
                    self.game_draw = True
                    print "GAME DRAW............................"
            else:
                if next.id_x == 7 or next.id_x == 0:
                    # a king was formed
                    # reset stale move count
                    self.game_draw_move_count = 0
                else:
                    # no_King_was_made
                    self.game_draw_move_count = self.game_draw_move_count + 1
                    if self.game_draw_move_count >= 100:  # when each player makes continuous 50 stale moves, game is draw
                        self.game_draw = True
                        print "GAME DRAW............................"

        next.setState(prior.state)
        prior.setState(STATES.coins.EMPTY)
        if isCapture:
            self.jumps_taken=self.jumps_taken+1
            self.capture(prior,next)
            self.forced_to_capture=next.isCapturePossible()
            if self.forced_to_capture:
                self.continue_capture=True
                self.forced_coin=next
                self.forced_coin.highlightCaptureMoves()
                self.players[self.turn].myTurn()
                return
            else:
                self.continue_capture=False
        self.total_move_count = self.total_move_count + 1
        self.board_pos_array.append(self.getCurrentBoardPosition())
        self.factors_array.append([self.findCoinsUnderThreatOfPlayer(self.turn), self.findCoinsUnderThreatOfPlayer((self.turn + 1) % 2), self.findImmovableCoins(self.turn), self.findImmovableCoins((self.turn + 1) % 2),self.jumps_taken])
        self.selected_a_checker=False
        self.selected_checker=None
        self.exchangeTurn()
        self.jumps_taken=0
        self.players[self.turn].myTurn()
        self.highlighted_squares=[]

    def moveCheckerVirtually(self,prior,next,isCapture=False):
        p = prior.id
        n = next.id
        cpos=self.getCurrentBoardPosition()
        pos=cpos
        pos[n] = pos[p]
        pos[p] = str(0)


        if isCapture:
            x = (prior.id_x + next.id_x) / 2
            y = (prior.id_y + next.id_y) / 2
            sq = self.findSquare(x, y)
            pos[sq.id]=str(0)

        return pos

    def isCapturePossibleForPlayer(self,id):
        flag= False
        color=self.players[id].getColor()
        for sq in self.squares:
            if sq.color==color:
                if sq.isCapturePossible():
                    flag= True
        return flag

    def exchangeTurn(self):
        self.turn = (self.turn + 1) % 2

    def findImmovableCoins(self,id):
        count=0
        color=self.players[id].getColor()
        side=self.players[id].side
        for i in range(0,32):
            if self.squares[i].color==color:
                if not self.squares[i].isMovable(side):
                    count=count+1
        #print "Immovable "+color+" coins = %d"%(count)
        return count

    def findCoinsUnderThreatOfPlayer(self, id):
        oponnent_id=id
        my_id=(id+1)%2
        if not self.isCapturePossibleForPlayer(oponnent_id):
            #print ("no cap possible for  "+self.players[oponnent_id].color)
            return 0
        else:
            count=0
            for i in range(0,32):
                if self.squares[i].color==self.players[my_id].getColor():
                    if self.squares[i].isUnderThreat():
                        count=count+1
            #print "%s coins under threat = %d" %(self.players[my_id].color,count)
            return count

    def findLongestJump(self):
        return 0

    def getCurrentBoardPosition(self):
        ####playyer #0 is considered as self and player #1 is considered as opponent
        board_pos=[]
        empty=STATES.coins.EMPTY
        my_coin=1
        my_king=2
        op_coin=3
        op_king=4

        if self.players[0].getColor() == STATES.color.WHITE:
            my_coin=STATES.coins.WHITE
            my_king=STATES.coins.WHITE_KING
            op_coin=STATES.coins.BLACK
            op_king=STATES.coins.BLACK_KING
        else:
            op_coin = STATES.coins.WHITE
            op_king = STATES.coins.WHITE_KING
            my_coin = STATES.coins.BLACK
            my_king = STATES.coins.BLACK_KING

        for i in range(0,32):
            if self.squares[i].state == empty:
                board_pos.append(str(0))
            elif self.squares[i].state == my_coin:
                board_pos.append(str(1))
            elif self.squares[i].state == my_king:
                board_pos.append(str(2))
            elif self.squares[i].state == op_coin:
                board_pos.append(str(3))
            elif self.squares[i].state == op_king:
                board_pos.append(str(4))
        return board_pos
