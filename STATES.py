from enum import Enum


class states(Enum):
    EMPTY=0
    OWN_COIN=1
    OWN_KING=2
    OPP_COIN=3
    OPP_KING=4

class coins(Enum):
    EMPTY = 1
    BLACK = 2
    WHITE = 3
    BLACK_KING = 4
    WHITE_KING = 5


class color(Enum):
    EMPTY="EMPTY"
    BLACK="BLACK"
    WHITE="WHITE"

class cointype(Enum):
    NORMAL=0
    KING=1

class player(Enum):
    HUMAN="HUMAN"
    COMPUTER="COMPUTER"

class side(Enum):
    TOP=0
    BOTTOM=1