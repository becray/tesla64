# README #

# NSS College of Engineering #
## Computer Science and Engineering Dept. ##
### Main Project 2013 - 2017 : Group - 14 ###


# Reinforcement Learning : Checkers. #

## Project Guide : Assoc. Prof. Sindhu S ##
## Project in-charge : Assoc. Prof. Anuraj Mohan M ##

## Members :- ##
### 1. Jithu Kiran K ###
### 2. Joel P Thomas ###
### 3. Mohammed Shibily T S ###
### 4. Sreeharsh Manas M M ###
### 5. Vivek Av ###

### How do I get set up? ###

Python 2.7 interpreter has to be set up.

### Libraries required :-

* Tkinter
* Scikit-learn
* Numpy + MKL
* PyBrain
* Pillow

You'll need to clone the repository and run it in [PyCharm IDE](https://www.jetbrains.com/pycharm/download/).